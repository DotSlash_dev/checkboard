package com.dotslash.checkboard;

import java.util.Arrays;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class CheckBoardDBHelper extends SQLiteOpenHelper
{
    private void createTable(SQLiteDatabase db, String name, String[] fields)
    {
        String fieldsListStr = Arrays.toString(fields);
        if (!fieldsListStr.equals("null"))
            fieldsListStr = fieldsListStr.substring(1, fieldsListStr.length() - 2);
        String query = "CREATE TABLE " + name + "(" + fieldsListStr + ");";
        db.execSQL(query);
    }

    public CheckBoardDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db)
    {
        String[] taskField = {
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "name TEXT",
            "description TEXT",
            "done INTEGER"
        };

        createTable(db, "task", taskField);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer)
    {
    }
}
