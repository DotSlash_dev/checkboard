package com.dotslash.checkboard;

import java.lang.String;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class CheckBoardActivity extends Activity implements DeleteItemDialogFragment.DialogListener
{
    CheckListAdapter data;
    SQLiteDatabase db;

    String listSizeLabel;
    String itemIdLabel;
    String itemNameLabel;
    String itemDescrLabel;
    String itemDoneLabel;

    private void getStrings()
    {
        listSizeLabel = getString(R.string.listSize);
        itemIdLabel = getString(R.string.itemId);
        itemNameLabel = getString(R.string.itemName);
        itemDescrLabel = getString(R.string.itemDescr);
        itemDoneLabel = getString(R.string.itemDone);
    }

    private void openDatabase()
    {
        CheckBoardDBHelper helper = new CheckBoardDBHelper(this, getString(R.string.app_name), null, 1);
        this.db = helper.getWritableDatabase();
    }

    private void initList(Bundle savedInstanceState)
    {
        this.data = new CheckListAdapter(this, R.layout.content);
        ListView list = (ListView) findViewById(R.id.content);
        list.setAdapter(data);

        if (savedInstanceState != null)
        {
            int size = savedInstanceState.getInt(listSizeLabel);
            for (int pos = 0; pos < size; pos++)
            {
                CheckListItem obj = new CheckListItem();
                obj.setId(savedInstanceState.getLong(itemIdLabel + pos));
                obj.setName(savedInstanceState.getString(itemNameLabel + pos));
                obj.setDescription(savedInstanceState.getString(itemDescrLabel + pos));
                obj.setDone(savedInstanceState.getBoolean(itemDoneLabel + pos));
                data.add(obj);
            }
        }
    }

    private void populateChecklist()
    {
        String query = "SELECT * from task";
        SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(query, null);
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                boolean done = cursor.getInt(cursor.getColumnIndex("done")) != 0;
                CheckListItem item = new CheckListItem(name, description);
                item.setId(id);
                item.setDone(done);
                data.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
    }

    private void createDefaultTasks()
    {
        Resources res = getResources();
        int defaultTasksNbr = res.getInteger(R.integer.start_nbr);
        String[] names = res.getStringArray(R.array.startItemNames);
        String[] descriptions = res.getStringArray(R.array.startItemDescriptions);

        for (int index = 0; index < defaultTasksNbr; index++)
        {
            String name = names[index];
            String description = descriptions[index];
            CheckListItem item = new CheckListItem(name, description);
            add(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getStrings();
        openDatabase();
        initList(savedInstanceState);
        if (savedInstanceState == null)
        {
            populateChecklist();
            if (data.getCount() == 0)
                createDefaultTasks();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        Adapter list = ((ListView) findViewById(R.id.content)).getAdapter();
        savedInstanceState.putInt(listSizeLabel, list.getCount());
        for (int pos = 0; pos < list.getCount(); pos++)
        {
            CheckListItem obj = (CheckListItem) list.getItem(pos);
            savedInstanceState.putLong(itemIdLabel + pos, obj.getId());
            savedInstanceState.putString(itemNameLabel + pos, obj.getName());
            savedInstanceState.putString(itemDescrLabel + pos, obj.getDescription());
            savedInstanceState.putBoolean(itemDoneLabel + pos, obj.isDone());
        }
    }

    @Override
    protected void onDestroy()
    {
        this.db.close();
        super.onDestroy();
    }

    private ContentValues getValues(CheckListItem item)
    {
        ContentValues values = new ContentValues(3);
        values.put("name", item.getName());
        values.put("description", item.getDescription());
        values.put("done", item.isDone() ? 1 : 0);
        System.out.println(item.isDone());
        return values;
    }

    private void add(CheckListItem item)
    {
        long id = db.insert("task", null, getValues(item));
        item.setId(id);
        data.add(item);
    }

    private void updateEntry(CheckListItem item)
    {
        String[] conditions = {String.valueOf(item.getId())};
        db.update("task", getValues(item), "id = ?", conditions);
    }

    private void deleteFromDB(CheckListItem item)
    {
        String[] conditions = {String.valueOf(item.getId())};
        db.delete("task", "id = ?", conditions);
    }

    private CheckListItem getItemFromView(View view, AdapterView adapter)
    {
        int position = adapter.getPositionForView(view);
        return data.getItem(position);
    }

    public void toggleDone(View doneCheckBox)
    {
        View parent = (View) doneCheckBox.getParent();
        AdapterView listView = (AdapterView) findViewById(R.id.content);
        CheckListItem item = getItemFromView(parent, listView);
        item.toggleDone();
        updateEntry(item);
    }

    public void addTask(View view)
    {
        ListView listView = (ListView) findViewById(R.id.content);
        CheckListItem newItem = new CheckListItem();
        add(newItem);
        listView.setSelection(data.getCount() - 1);
    }

    private void invertVisibility(View view)
    {
        int currentVisilibity = view.getVisibility();
        view.setVisibility(currentVisilibity == View.GONE ? View.VISIBLE : View.GONE);
    }

    private void invertContentVisibility(ViewGroup viewGroup)
    {
        for (int i = 0; i < viewGroup.getChildCount(); i++)
            invertVisibility(viewGroup.getChildAt(i));
    }

    private void toggleEditMode(ViewGroup itemView)
    {
        View done = itemView.findViewById(R.id.done);
        ViewGroup taskInfo = (ViewGroup) itemView.findViewById(R.id.taskInfo);
        ViewGroup options = (ViewGroup) itemView.findViewById(R.id.options);

        done.setVisibility(done.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
        invertContentVisibility(taskInfo);
        invertContentVisibility(options);
    }

    public void enableEditMode(View widget)
    {
        ViewGroup view = (ViewGroup) widget.getParent().getParent();
        enableEditMode(view);
    }

    public void enableEditMode(ViewGroup view)
    {
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView description = (TextView) view.findViewById(R.id.description);
        EditText nameEdit = (EditText) view.findViewById(R.id.name_edit);
        EditText descrEdit = (EditText) view.findViewById(R.id.descr_edit);

        toggleEditMode(view);
        nameEdit.setText(name.getText());
        descrEdit.setText(description.getText());
    }

    public void disableEditMode(View widget)
    {
        ViewGroup view = (ViewGroup) widget.getParent().getParent();
        toggleEditMode(view);
    }

    public void disableEditMode(ViewGroup view) { toggleEditMode(view); }

    public void saveItem(View saveButton)
    {
        ListView list = (ListView) findViewById(R.id.content);
        ViewGroup view = (ViewGroup) saveButton.getParent().getParent();
        CheckListItem item = getItemFromView(view, list);
        EditText nameEdit = (EditText) view.findViewById(R.id.name_edit);
        EditText descrEdit = (EditText) view.findViewById(R.id.descr_edit);

        item.setName(nameEdit.getText().toString());
        item.setDescription(descrEdit.getText().toString());
        updateEntry(item);
        disableEditMode(view);
    }

    public void deleteItem(View deleteButton)
    {
        ListView list = (ListView) findViewById(R.id.content);
        ViewGroup view = (ViewGroup) deleteButton.getParent().getParent();
        CheckListItem item = getItemFromView(view, list);

        DeleteItemDialogFragment confirm = new DeleteItemDialogFragment();
        confirm.setItemId(item.getId());
        confirm.show(getFragmentManager(), "removeItem");
    }

    public void onPositiveResponse(DialogFragment fragment)
    {
        DeleteItemDialogFragment dialog = (DeleteItemDialogFragment) fragment;
        CheckListItem item = data.findById(dialog.getItemId());
        deleteFromDB(item);
        data.remove(item);
    }

    public void onNegativeResponse(DialogFragment dialog) {}
}
