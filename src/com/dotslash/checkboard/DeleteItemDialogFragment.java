package com.dotslash.checkboard;
import com.dotslash.checkboard.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class DeleteItemDialogFragment extends DialogFragment
{
    public interface DialogListener
    {
        public void onPositiveResponse(DialogFragment dialog);
        public void onNegativeResponse(DialogFragment dialog);
    }
    
    long itemId;
    DialogListener listener;

    public long getItemId() { return itemId; }
    public void setItemId(long id) { this.itemId = id; }

    DialogInterface.OnClickListener accept = new DialogInterface.OnClickListener()
    {
        public void onClick(DialogInterface dialog, int which)
        {
            listener.onPositiveResponse(DeleteItemDialogFragment.this);
        }
    };

    DialogInterface.OnClickListener refuse = new DialogInterface.OnClickListener()
    {
        public void onClick(DialogInterface dialog, int which)
        {
            listener.onNegativeResponse(DeleteItemDialogFragment.this);
        }
    };

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.delItemTitle);
        builder.setMessage(R.string.delItemMessage);
        builder.setPositiveButton(R.string.delItemOK, accept);
        builder.setNegativeButton(R.string.delItemCancel, refuse);
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        this.listener = (DialogListener) activity;
    }
}
