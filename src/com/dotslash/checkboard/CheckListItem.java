package com.dotslash.checkboard;

class CheckListItem
{
    long dbId;
    String name;
    String description;
    boolean done = false;

    public CheckListItem()
    {
    }

    public CheckListItem(String name)
    {
        setName(name);
    }

    public CheckListItem(String name, String description)
    {
        setName(name);
        setDescription(description);
    }

    public long getId() { return this.dbId; }
    public void setId(long id) { this.dbId = id; }

    public String getName() { return this.name; }
    public boolean hasName() { return !(this.name == null || this.name.isEmpty()); }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return this.description; }
    public void setDescription(String description) { this.description = description; }

    public boolean isDone() { return this.done; }
    public void setDone(boolean state) { this.done = state; }
    public void toggleDone() { this.done = !done; }
}
