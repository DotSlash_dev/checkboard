package com.dotslash.checkboard;
import com.dotslash.checkboard.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

class CheckListAdapter extends ArrayAdapter<CheckListItem>
{
    public CheckListAdapter(Context context, int resourceId)
    {
        super(context, resourceId);
    }

    public CheckListItem findById(long id)
    {
        for (int position = 0; position < getCount(); position++)
        {
            CheckListItem item = getItem(position);
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    public View getView(int pos, View view, ViewGroup parent)
    {
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.content, null);
        }
        CheckListItem item = getItem(pos);
        if (item != null)
        {
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView description = (TextView) view.findViewById(R.id.description);
            CheckBox done = (CheckBox) view.findViewById(R.id.done);

            name.setText(item.getName());
            description.setText(item.getDescription());
            done.setChecked(item.isDone());
        }
        return view;
    }
}
